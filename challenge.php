<?php

# challenge 1
function getPiDecimal(){
# your code here
	$pi = pi();
	
	$piDecimal = str_split(strval($pi));

	return $piDecimal[9];
}

# challenge 2
function getSumEvens($a=[1,2,3,4,5,6]){
# your code here
	$sumEvens = 0;
	$numPar = 2;
	foreach ($a as $name => $value)
	{
		$value % $numPar == 0  ? $sumEvens = $sumEvens + $value : 0 ;
	}
	return $sumEvens;
}

# challenge 3
function getOrderedVowels($s="just a testing"){
# your code here

	$vowels = '';
	$orderVowels = str_split($s);
	foreach ($orderVowels as $name => $value)
	{
		switch ($value){
			case 'a': 
				$vowels .= $value;
				break;
			case 'e': 
				$vowels .= $value;
				break;
			case 'i': 
				$vowels .= $value;
				break;
			case 'o': 
				$vowels .= $value;
				break;
			case 'u': 
				$vowels .= $value;
				break;	
		}
	}
	
	return $vowels;
}

# challenge 4
# obtener el primer id del JSON : https://jsonplaceholder.typicode.com/users
function getFirstId(){
# your code here
	$data = file_get_contents("https://jsonplaceholder.typicode.com/users");
	$users = json_decode($data, true);

	//print_r($users[0]['id']);

	return $users[0]['id'];
}

# DONT EDIT
echo "Running: \n";
echo "challenge 1: ".((getPiDecimal()==5)? "pass" : "fail") . "\n" ;
echo "challenge 2: ".((getSumEvens()==12)? "pass" : "fail" ) . "\n" ;
echo "challenge 3: ".((getOrderedVowels()=="uaei")? "pass" : "fail") . "\n" ;
echo "challenge 4: ".((getFirstId()==1)? "pass" : "fail" ).		 "\n" ;
