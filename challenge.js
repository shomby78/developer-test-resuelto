// challenge 1
function getPiDecimal(){
// your code here
	var pi = Math.PI.toString();
	var piDecimal = pi.charAt(9);

	if (piDecimal == 5) {
		console.log("challenge 1: pass");
	} else { 
		console.log("challenge 1: fail");
	}
}

// challenge 2
function getSumEvens(a=[1,2,3,4,5,6]){
// your code here
	var sumEvens = 0;
	var numPar = 2;

	for (i = 0; i < a.length; i++) {
		if (a[i] % numPar == 0) 
			sumEvens += a[i];
	}
	
	if (sumEvens == 12) {
		console.log("challenge 2: pass");
	} else { 
		console.log("challenge 2: fail");
	}
}

// challenge 3
function getOrderedVowels(s="just a testing"){
// your code here
	var vowels = "";

	for (i = 0; i < s.length; i++) {
		switch(s.charAt(i)) {
			case "a":
				vowels += s.charAt(i);
				break;
			case "e":
				vowels += s.charAt(i);
				break;
			case "i":
				vowels += s.charAt(i);
				break;
			case "o":
				vowels += s.charAt(i);
				break;
			case "u":
				vowels += s.charAt(i);
				break;			
		}
	}
	if (vowels == "uaei") {
		console.log("challenge 3: pass");
	} else { 
		console.log("challenge 3: fail");
	}
}

// challenge 4
// obtener el primer id del JSON : https://jsonplaceholder.typicode.com/users
function getFirstId(){
// your code here

	var firstId = 0;
	
	fetch('https://jsonplaceholder.typicode.com/users').then( function(res) {
		return res.json()
	}).then( function(data){
			firstId = parseInt(data[0].id);
		})
	if (firstId !== 1) {
		console.log("challenge 4: pass");
	} else { 
		console.log("challenge 4: fail");
	}

}



	

// DONT EDIT
console.log("Running: \n");
getPiDecimal();
getSumEvens();
getOrderedVowels();
getFirstId();
