- 1) que tanto conoces sobre sistemas ERP?
    En mi ultimo empleo, trabajamos con un ERP "In house", llamado CAIN, el cual llevaba el manejo de todas las operaciones de la empresa: ordenes de compra, cuentas por pagar, cuentas por cobrar, facturacion, contratos, personal, gestion de hotel, gestion financiera, gestion de documentos, entre otros. Cada una de estas partes representaba un modulo del sistema, estaba construido bajo varias capas y tecnolgias, todos entorno al lenguaje JAVA.
- 2) que tanto conoces sobre facturacion electronica?
    La verdad nunca he heco un sistema de facturacion electronico, en el sistema CAIN se genera la factura y se enviaba a los clientes via correo en formato pdf, es lo mas cercano a facturacion electronica que he estado. 
- 3) que tanto conoces sobre procesos de CI/CD?
    Este es el dia a dia en mi anterior trabajo, usando tecnologias como SVN y metodologias de trabajo como SCRUM, se hacia en el sistema despliegues controlados para hacer mejoras y despliegues criticos para corregir fallas. Lleve el departamento de operatividad por cerca de seis meses, en el cual se manejaban las fallas del sistema y los despliegues criticos del mismo. 
- 4) que tanto conoces sobre linux?
    Nociones basicas para ser honesto.
- 5) que te gusta y NO te gusta del lenguaje que usas?
    Del lenguaje PHP me gusta todo lo que conozco, aun no he encontado alguna limitante que me disguste.
- 6) si llegas a un punto con algo que no sabes como solucionar que haces?
    Como pimera opcion consulto foros especializados y paginas como stackoverflow, como segunda opcion consulto a mis companeros de trabajo y a mis amigos colegas por las redes sociales.
- 7) que opinas del trabajo remoto, tienes experiencia?
    Me parece una buena metodologia de trabajo, he tenido experiencia durante el tiempo que estuve freelance, llevando varios proyectos en diferentes ciudades y paises. En esta forma de trabajo lo importante es tener una excelente comunicacion.
- 8) si te dan una instruccion que no comprendes que haces?
    Sigo preguntando hasta lograr la comprension total de las instrucciones dadas, esto evita la perdida de tiempo y el retrabajo.
- 9) que opinas de las "horas extras"?
    Cuendo son necesarias para lograr los tiempos de entrega se deben tomar, es importante siempre cumplir los tiempos, pero trato de organizar mi trabajo para llevarlo lo mas al dia posible.
- 10) cuentame cual es la cosa mas dificil que has resuelto.
    Una de las labores mas complicados que he resuelto, es con respecto al envio de pagos electronicos a proveedores. El sistma genera un archivo de texto con todas las transferencias a bancos de los distintos proveedores de la empresa. Esto estaba desarrollado para Venezuela, y el trabajo que realice fue, sin modificar lo existente, desarrollar el modulo para los pagos de Costa Rica. Fue un trabajo delicado y de mucho detalle, pero fue un trabajo bien hecho, salvo pocos detalles que salieron despues del despliegue a produccion. 