# challenge 1
def getPiDecimal():
# your code here
	import math

	pi = str(math.pi)
	
	return int(pi[9])

# challenge 2
def getSumEvens(a=[1,2,3,4,5,6]):
# your code here
	sumEvens = 0
	numPar = 2
	
	for value in a:
		 if value % numPar == 0:
		 	sumEvens = sumEvens + value
	
	return sumEvens


# challenge 3
def getOrderedVowels(s="just a testing"):
# your code here
	vowels = ''

	for value in s:
		if value == 'a':
			vowels += value
		elif value == 'e':
			vowels += value
		elif value == 'i':
			vowels += value
		elif value == 'o':
			vowels += value
		elif value == 'u':
			vowels += value

	return vowels


# challenge 4
def getFirstId():
# yor code here
	import requests
	
	response = requests.get('http://jsonplaceholder.typicode.com/users')
	data = response.json()

	return data[0]['id']


# DONT EDIT
print ("Running: ")
print ("challenge 1: "+("pass" if (getPiDecimal()==5) else "fail"))  
print ("challenge 2: "+("pass" if (getSumEvens()==12) else "fail" ))  
print ("challenge 3: "+("pass" if (getOrderedVowels()=="uaei") else "fail"))  
print ("challenge 4: "+("pass" if (getFirstId()==1) else "fail" )) 